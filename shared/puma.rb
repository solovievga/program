#!/usr/bin/env puma

directory '/home/solovievga/apps/program/current'
rackup "/home/solovievga/apps/program/current/config.ru"
environment 'development'

tag ''

pidfile "/home/solovievga/apps/program/shared/tmp/pids/puma.pid"
state_path "/home/solovievga/apps/program/shared/tmp/pids/puma.state"
stdout_redirect '/home/solovievga/apps/program/current/log/puma.error.log', '/home/solovievga/apps/program/current/log/puma.access.log', true


threads 4,16



bind 'unix:///home/solovievga/apps/program/shared/tmp/sockets/puma.sock'

workers 1





preload_app!


on_restart do
  puts 'Refreshing Gemfile'
  ENV["BUNDLE_GEMFILE"] = ""
end


before_fork do
  ActiveRecord::Base.connection_pool.disconnect!
end

on_worker_boot do
  ActiveSupport.on_load(:active_record) do
    ActiveRecord::Base.establish_connection
  end
end

