import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(posts: any, search: any): any {
  if(search === undefined) return posts;

    return posts.filter(function(post)
    {
    return post.name.toLowerCase().includes(search.toLowerCase());
    })
  }

}
