export class Post {
  constructor(
    public id?: number,
    public name?: string,
    public phone?: string,
    public work?: string,
    public position?: string,
    public mail?: string,
    public inst?: string,
    public vk?: string,
    public facebook?: string,
    public twitter?: string,
    public telegram?: string,
    public addres?: string,
    public updated_at?: string,
  ) {}
}
